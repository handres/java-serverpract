import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.Naming;

public class MensajesClient {
	public static void main(String[] args) {
		
		String mensaje ="";
		try {
			Mensajes c = (Mensajes) Naming.lookup("rmi://localhost/MensajesService");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			do {
				mensaje = in.readLine();
				c.escribe(mensaje);
			} while (!mensaje.startsWith("fin"));
		}catch (Exception e) {
			System.out.println("Problema: "+e);
		}
	}
}
