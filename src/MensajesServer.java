import java.rmi.Naming;
public class MensajesServer { 
	
	public MensajesServer() {
		try {
			Mensajes c = new MensajesImpl();
			Naming.rebind("rmi://localhost:1099/MensajesService", c);
		} catch (Exception e) {
			System.out.println("Problema: " + e);
		}
	}
	
	public static void main(String args[]) {
		new MensajesServer();
	}
}